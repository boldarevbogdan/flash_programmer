/*
 * spi_flash.cpp
 *
 *  Created on: 5 февр. 2018 г.
 *      Author: bogdan
 */

#include "spi_flash.h"

uint8_t SpiFlashReadStatus (void)
{
	uint8_t status(0);

	CSControl (LOW);

	uint8_t data = ReverseBitsOrder (FLASH_READ_STATUS);

	SPIWrite (&data, 1);

	SPIRead (&status, 1);

	status = ReverseBitsOrder (status);

	CSControl (HIGH);

	return status;
}

void SpiFlashWriteEnable (void)
{
	CSControl (LOW);

	uint8_t data = ReverseBitsOrder (FLASH_WRITE_ENABLE);

	SPIWrite (&data, 1);

	CSControl (HIGH);
}

void SpiFlashGlobalUnprotect (void)
{
	SpiFlashWriteEnable ();

	CSControl (LOW);

	uint8_t data = ReverseBitsOrder (FLASH_GLOBAL_UNPROTECT);

	SPIWrite (&data, 1);

	CSControl (HIGH);
}

uint8_t SpiFlashChipErase (void)
{
	uint16_t erase_timeout(0);

	SpiFlashGlobalUnprotect ();

	SpiFlashWriteEnable ();

	CSControl (LOW);

	uint8_t data = ReverseBitsOrder (FLASH_CHIP_ERASE);

	SPIWrite (&data, 1);

	CSControl (HIGH);

	while (erase_timeout < 5000)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(10));

		if (!(SpiFlashReadStatus() & FLASH_BUSY))
			return 0;

		erase_timeout++;
	}

	return 1;
}

uint8_t SpiFlashPageProgramInverted16 (const uint32_t page_address, const uint8_t start_byte, uint16_t num_halfwords, uint16_t *array)
{
	uint8_t *write_buf = new uint8_t[num_halfwords * 2];
	uint8_t *buf_write_index = write_buf;
	bool write_success = false;

	SpiFlashWriteEnable ();

	for (uint16_t i = 0; i < num_halfwords; i++)
	{
		*buf_write_index++ = (uint8_t)array[i];
		*buf_write_index++ = (uint8_t)(array[i] >> 8);
	}

	uint16_t crc1 = 0;
	crc1 = crc16 (crc1, write_buf, (num_halfwords * 2));

	uint8_t address_buf[4]{0};

	address_buf[0] = ReverseBitsOrder (FLASH_PROGRAM_PAGE);
	address_buf[1] = ReverseBitsOrder ((uint8_t) (page_address >> 8));
	address_buf[2] = ReverseBitsOrder ((uint8_t) (page_address));
	address_buf[3] = ReverseBitsOrder (start_byte);

	CSControl (LOW);

	SPIWrite (address_buf, 4);

	SPIWrite (write_buf, (num_halfwords * 2));

	CSControl (HIGH);

	delete[] write_buf;

	uint8_t timeout = 0;

	while ((timeout < 100) && (write_success == false))
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1));

		if (!(SpiFlashReadStatus() & FLASH_BUSY))
			write_success = true;

		timeout++;
	}

	if (timeout >= 100)
		return 1;

	uint8_t *read_buf = new uint8_t[num_halfwords * 2];

	address_buf[0] = ReverseBitsOrder (FLASH_READ);
	address_buf[1] = ReverseBitsOrder ((uint8_t) (page_address >> 8));
	address_buf[2] = ReverseBitsOrder ((uint8_t) (page_address));
	address_buf[3] = ReverseBitsOrder (start_byte);

	CSControl (LOW);

	SPIWrite (address_buf, 4);

	SPIRead (read_buf, (num_halfwords * 2));

	CSControl (HIGH);

	uint16_t crc2(0);
	crc2 = crc16 (crc2, read_buf, (num_halfwords * 2));

	delete[] read_buf;

	if (crc1 != crc2)
		return 2;

	return 0;
}

void WriteImageToFlash (const Image_Typedef * Image, const std::string name)
{
	uint16_t page_address = (uint16_t)(Image->image_address >> 8);
	uint8_t start_byte = (uint8_t)(Image->image_address);
	uint32_t num_halfwords = Image->x_size * Image->y_size;
	uint32_t i(0);		/* переменная которая хранит сколько полуслов уже записано */
	uint8_t ret(0);
	uint8_t result(0);
	/*
	 * рассчитываем сколько полуслов (2 байта) осталось до конца текущей страницы
	 */
	uint8_t num_halfwords_temp = (256 - start_byte) / 2;

	if (num_halfwords_temp >= num_halfwords)
	{
		ret = SpiFlashPageProgramInverted16 (page_address, start_byte, num_halfwords, (uint16_t *)(Image->buf));
		if (ret != 0)
		{
			result = ret;
			goto error;
		}
		else
			goto success;
	}

	ret = SpiFlashPageProgramInverted16 (page_address, start_byte, num_halfwords_temp, (uint16_t *)(Image->buf)); 	/* записываем данные до конца текущей страницы */
	if (ret != 0)
	{
		result = ret;
		goto error;
	}

	page_address++;
	num_halfwords = num_halfwords - num_halfwords_temp;

	i = i + num_halfwords_temp;

	while(num_halfwords > 128)
	{
		uint8_t ret = SpiFlashPageProgramInverted16 (page_address, 0, 128, (uint16_t *)(Image->buf + i));
		if (ret != 0)
		{
			result = ret;
			goto error;
		}

		num_halfwords = num_halfwords - 128;
		page_address++;
		i = i + 128;
	}

	ret = SpiFlashPageProgramInverted16 (page_address, 0, num_halfwords, (uint16_t *)(Image->buf + i));
	if (ret != 0)
	{
		result = ret;
		goto error;
	}

	success:
	std::cout << "Image " << name << " was written successfully." << std::endl;
	return;

	error:
	std::cout << "Error while written " << name << " image." << std::endl;
	std::cout << "Error code is " << std::to_string(result) << std::endl;
	USBSPIConverterDeinit ();
	exit (1);
}
