/*
 * ch341a.cpp
 *
 *  Created on: 21 янв. 2018 г.
 *      Author: bogdan
 */

#include <usbspiconverter.h>

struct libusb_device_handle *devHandle = NULL;
struct libusb_context *context = NULL;

void sigint_handler(int signal);

int8_t USBSPIConverterInit (void)
{
	std::signal(SIGINT, sigint_handler);

	if (libusb_init(&context) < 0)
	{
		std::cout << "Couldn't initialise libusb." << std::endl;
		return -1;
	}

	libusb_set_debug(NULL, 3);

#ifdef CH341A
	devHandle = libusb_open_device_with_vid_pid(NULL, CH341A_VID, CH341A_PID);
#endif
#ifdef STM32F1_CONVERTER
	devHandle = libusb_open_device_with_vid_pid(NULL, STM32F1_CONVERTER_VID, STM32F1_CONVERTER_PID);
#endif

	if (devHandle == 0)
	{
		std::cout << "Couldn't open device." << std::endl;
		return -2;
	}

	libusb_device *dev = libusb_get_device(devHandle);

	if (dev == 0)
	{
		std::cout << "Couldn't get bus number and address." << std::endl;
		return -3;
	}

    if(libusb_kernel_driver_active(devHandle, 0))
    {
        auto ret = libusb_detach_kernel_driver(devHandle, 0);
        if (ret)
        {
        	std::cout << "Failed to detach kernel driver." << std::endl;
        	libusb_release_interface(devHandle, 0);
        	libusb_close(devHandle);
        	return -4;
        }
    }

    {
		auto ret = libusb_claim_interface(devHandle, 0);
		if (ret)
		{
			std::cout << "Failed to claim interface." << std::endl;
			libusb_release_interface(devHandle, 0);
			libusb_close(devHandle);
			return -5;
		}
    }

#ifdef STM32F1_CONVERTER
    int ret = 0;
    int transfered = 0;
    uint8_t data_buf[2]{0};

    data_buf[0] = USB_DEVICE_SET_SPEED;
    data_buf[1] = USB_DEVICE_SPI_SPEED_18MHZ;

    ret = libusb_bulk_transfer(devHandle, COMMAND_ENDPOINT, data_buf, 2, &transfered, DEFAULT_TIMEOUT);
	if (ret < 0)
		return ret;

    data_buf[0] = USB_DEVICE_SET_BIT_ORDER;
    data_buf[1] = USB_DEVICE_SPI_LSBFIRST;

    ret = libusb_bulk_transfer(devHandle, COMMAND_ENDPOINT, data_buf, 2, &transfered, DEFAULT_TIMEOUT);
	if (ret < 0)
		return ret;
#endif

	return 0;
}

void USBSPIConverterDeinit (void)
{
	libusb_release_interface(devHandle, 0);
	libusb_close(devHandle);
	libusb_exit(context);
	devHandle = NULL;
}

int USBTransfer (const char *func, uint8_t endpoint, uint8_t *data_buf, uint8_t bytes_num)
{
	int ret = 0;
	int transfered = 0;

	ret = libusb_bulk_transfer(devHandle, endpoint, data_buf, bytes_num, &transfered, DEFAULT_TIMEOUT);
	if (ret < 0)
		return ret;

	return transfered;
}

/* управление пином СS. state - состояние пина, 1 - подтянуть к питанию, 0 - подтянуть к земле */
int CSControl (uint8_t state)
{
	uint8_t data_buf[4]{0};
	int ret(0);
	int transfered(0);

#ifdef CH341A

	uint8_t bytes_to_send(0);
	uint8_t *data_p = data_buf;

	*data_p++ = CH341A_CMD_UIO_STREAM;

	if (state == 0)
	{
		*data_p++ = CH341A_CMD_UIO_STM_OUT | 0x36;
		*data_p++ = CH341A_CMD_UIO_STM_DIR | 0x3F;

		bytes_to_send = 4;
	}
	else
	{
		*data_p++ = CH341A_CMD_UIO_STM_OUT | 0x37;

		bytes_to_send = 3;
	}

	*data_p++ = CH341A_CMD_UIO_STM_END;

	ret = libusb_bulk_transfer(devHandle, BULK_WRITE_ENDPOINT, data_buf, bytes_to_send, &transfered, DEFAULT_TIMEOUT);
	if (ret < 0)
		return ret;
#endif

#ifdef STM32F1_CONVERTER
	data_buf[0] = USB_DEVICE_SPI_SET_CS_STATE;
	data_buf[1] = state;

	ret = libusb_bulk_transfer(devHandle, COMMAND_ENDPOINT, data_buf, 2, &transfered, DEFAULT_TIMEOUT);
	if (ret < 0)
		return ret;
#endif

	return transfered;
}


int SPIWrite (uint8_t *write_data, uint32_t bytes_num)
{
	int ret(0);
	int transfered(0);

	uint8_t *data_buf_read_position = write_data;

#ifdef CH341A
	uint8_t buf[CH341_PACKET_LENGTH]{0};
	uint8_t read_buf[CH341_PACKET_LENGTH - 1]{0};

	while (bytes_num > CH341_PACKET_LENGTH - 1)
	{
		buf[0] = CH341A_CMD_SPI_STREAM;
		for (uint8_t i = 0; i < CH341_PACKET_LENGTH - 1; i++)
			buf[i + 1] = *data_buf_read_position++;

		ret = USBTransfer (__FUNCTION__, BULK_WRITE_ENDPOINT, buf, CH341_PACKET_LENGTH);
		if (ret < 0)
			return 0;

		ret = USBTransfer (__FUNCTION__, BULK_READ_ENDPOINT, read_buf, CH341_PACKET_LENGTH - 1);

		if (ret < 0)
			return 0;

		bytes_num = bytes_num - (CH341_PACKET_LENGTH - 1);
	}

	buf[0] = CH341A_CMD_SPI_STREAM;
	for (uint8_t i = 0; i < bytes_num; i++)
		buf[i + 1] = *data_buf_read_position++;

	ret = USBTransfer (__FUNCTION__, BULK_WRITE_ENDPOINT, buf, (bytes_num + 1));

	if (ret < 0)
		return 0;

	ret = USBTransfer (__FUNCTION__, BULK_READ_ENDPOINT, read_buf, CH341_PACKET_LENGTH - 1);

	if (ret < 0)
		return 0;
#endif

#ifdef STM32F1_CONVERTER
	uint8_t buf[STM32F1_CONVERTER_PACKET_LENGTH]{0};

	while (bytes_num > STM32F1_CONVERTER_PACKET_LENGTH)
	{
		for (uint8_t i = 0; i < STM32F1_CONVERTER_PACKET_LENGTH; i++)
			buf[i] = *data_buf_read_position++;

		ret = libusb_bulk_transfer(devHandle, DATA_OUT_ENDPOINT, buf, STM32F1_CONVERTER_PACKET_LENGTH, &transfered, DEFAULT_TIMEOUT);
		if (ret < 0)
			return 0;

		bytes_num -= STM32F1_CONVERTER_PACKET_LENGTH;
	}

	for (uint8_t i = 0; i < bytes_num; i++)
		buf[i] = *data_buf_read_position++;

	ret = libusb_bulk_transfer(devHandle, DATA_OUT_ENDPOINT, buf, bytes_num, &transfered, DEFAULT_TIMEOUT);
	if (ret < 0)
		return 0;
#endif

	return transfered;
}

int SPIRead (uint8_t *read_data, uint32_t bytes_num)
{
	int transfered(0);
	int ret(0);

	uint8_t *data_buf_write_position = read_data;

#ifdef CH341A
	uint8_t buf[CH341_PACKET_LENGTH - 1]{0};
	uint8_t write_buf[CH341_PACKET_LENGTH]{0};

	while (bytes_num > CH341_PACKET_LENGTH - 1)
	{
		write_buf[0] = CH341A_CMD_SPI_STREAM;

		ret = USBTransfer (__FUNCTION__, BULK_WRITE_ENDPOINT, write_buf, CH341_PACKET_LENGTH);
		if (ret < 0)
			return 0;

		ret = USBTransfer (__FUNCTION__, BULK_READ_ENDPOINT, buf, CH341_PACKET_LENGTH - 1);
		if (ret < 0)
			return 0;

		for (uint8_t i = 0; i < (CH341_PACKET_LENGTH - 1); i++)
			*data_buf_write_position++ = buf[i];

		bytes_num = bytes_num - (CH341_PACKET_LENGTH - 1);
	}

	write_buf[0] = CH341A_CMD_SPI_STREAM;

	ret = USBTransfer (__FUNCTION__, BULK_WRITE_ENDPOINT, write_buf, (bytes_num + 1));

	if (ret < 0)
		return 0;

	ret = USBTransfer (__FUNCTION__, BULK_READ_ENDPOINT, buf, bytes_num);

	if (ret < 0)
		return 0;

	for (uint8_t i = 0; i < ret; i++)
		*data_buf_write_position++ = buf[i];
#endif

#ifdef STM32F1_CONVERTER
	uint8_t buf[STM32F1_CONVERTER_PACKET_LENGTH]{0};

	while (bytes_num > STM32F1_CONVERTER_PACKET_LENGTH)
	{
		ret = libusb_bulk_transfer(devHandle, DATA_OUT_ENDPOINT, buf, STM32F1_CONVERTER_PACKET_LENGTH, &transfered, DEFAULT_TIMEOUT);
		if (ret < 0)
			return 0;

		std::this_thread::sleep_for(std::chrono::microseconds(10));

		ret = libusb_bulk_transfer(devHandle, DATA_IN_ENDPOINT, buf, STM32F1_CONVERTER_PACKET_LENGTH, &transfered, DEFAULT_TIMEOUT);
		if (ret < 0)
			return 0;

		for (uint8_t i = 0; i < STM32F1_CONVERTER_PACKET_LENGTH; i++)
		{
			*data_buf_write_position++ = ReverseBitsOrder(buf[i]);
			buf[i] = 0;
		}

		bytes_num -= STM32F1_CONVERTER_PACKET_LENGTH;
	}

	ret = libusb_bulk_transfer(devHandle, DATA_OUT_ENDPOINT, buf, bytes_num, &transfered, DEFAULT_TIMEOUT);
	if (ret < 0)
		return 0;

	std::this_thread::sleep_for(std::chrono::microseconds(10));

	ret = libusb_bulk_transfer(devHandle, DATA_IN_ENDPOINT, buf, bytes_num, &transfered, DEFAULT_TIMEOUT);
	if (ret < 0)
		return 0;

	for (uint8_t i = 0; i < bytes_num; i++)
		*data_buf_write_position++ = ReverseBitsOrder(buf[i]);
#endif

	return transfered;
}

uint8_t ReverseBitsOrder (uint8_t byte)
{
	uint8_t reversed_byte(0);

	for (uint8_t i = 0; i < 8; i++)
	{
		if (byte & (1 << i))
			reversed_byte |= 1 << (7 - i);
	}

	return reversed_byte;
}

void sigint_handler(int signal)
{
	if (signal == SIGINT)
	{
		std::cout << "SIGINT received!" << std::endl;
		USBSPIConverterDeinit ();
		exit(1);
	}
}


