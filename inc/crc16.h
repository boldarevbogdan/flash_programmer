/*
 * crc16.h
 *
 *  Created on: 21 янв. 2018 г.
 *      Author: bogdan
 */

#ifndef CRC16_H_
#define CRC16_H_

#include <cstdint>

extern const uint16_t crc16_table[256];

inline uint16_t crc16_byte(uint16_t crc, const uint8_t data)
{
	return (crc >> 8) ^ crc16_table[(crc ^ data) & 0xff];
}

uint16_t crc16(uint16_t crc, const uint8_t *buffer, uint32_t len);

#endif /* CRC16_H_ */
