/*
 * spi_flash.h
 *
 *  Created on: 5 февр. 2018 г.
 *      Author: bogdan
 */

#ifndef SPI_FLASH_H_
#define SPI_FLASH_H_

#include <usbspiconverter.h>
#include <iostream>
#include <thread>
#include <chrono>
#include <string>
#include <cstdlib>
#include "images.h"
#include "crc16.h"

#define FLASH_READ_STATUS			0x05
#define FLASH_WRITE_ENABLE 			0x06
#define FLASH_GLOBAL_UNPROTECT		0x98
#define FLASH_CHIP_ERASE			0xC7	/* TODO требуется уточнить команды */
#define FLASH_PROGRAM_PAGE			0x02
#define FLASH_READ					0x03

#define FLASH_BUSY					(1 << 0)

void SpiFlashWriteEnable (void);

/* возвращает 0 в случае успеха и 1 если таймаут истек */
uint8_t SpiFlashChipErase (void);

/* записываем изображение во флеш */
void WriteImageToFlash (const Image_Typedef *Image, const std::string name);

#define WRITE_IMAGE(x)	WriteImageToFlash(&x, #x)

#endif /* SPI_FLASH_H_ */
