/*
 * pictures.h
 *
 *  Created on: 10 окт. 2017 г.
 *      Author: bogdan
 */

#ifndef IMAGES_H_
#define IMAGES_H_

#ifdef __cplusplus
	#include <cstdint>

	extern "C" {
#else
	#include <stdint.h>
#endif

typedef struct {
	const uint16_t * buf;
	uint32_t image_address;
	uint16_t x_size;
	uint16_t y_size;
} Image_Typedef;

extern const Image_Typedef mainPSun;
extern const Image_Typedef mainPMon;
extern const Image_Typedef mainPTue;
extern const Image_Typedef mainPWed;
extern const Image_Typedef mainPThu;
extern const Image_Typedef mainPFri;
extern const Image_Typedef mainPSat;
extern const Image_Typedef mainPDay;				/* надписи День Ночь позиция (78, 12) */
extern const Image_Typedef mainPNight;
extern const Image_Typedef mainPSunny;				/* солнышко и снежинка позиция (125, 4) */
extern const Image_Typedef mainPSnowflake;
extern const Image_Typedef mainPProfileComfort;		/* профили позиция (195, 2) */
extern const Image_Typedef mainPProfileEco;
extern const Image_Typedef mainPProfileProtect;
extern const Image_Typedef mainPFan0;				/* кнопка переключения вентилятора позиция (204, 58) */
extern const Image_Typedef mainPFan1;
extern const Image_Typedef mainPFan2;
extern const Image_Typedef mainPFan3;
extern const Image_Typedef mainPFanOff;				/* подпись режимов вентилятора позация (177, 147) */
extern const Image_Typedef mainPFanManual;
extern const Image_Typedef mainPFanAuto;
extern const Image_Typedef mainPScale;				/* шкала позиция (53, 192) */
extern const Image_Typedef mainPPlusButton;			/* кнопка + позиция (277, 194) */
extern const Image_Typedef mainPPlusButtonPressed;	/* кнопка + нажатая (266, 186) */
extern const Image_Typedef mainPMinusButton;		/* кнопка + позиция (??, 194) */
extern const Image_Typedef mainPMinusButtonPressed;	/* кнопка - нажатая (0, 186) */
extern const Image_Typedef mainPSlider;				/* кнопка + позиция (??, 194) */
extern const Image_Typedef mainPFanSmall;			/* значок вентилятора позиция (107, 160) */
extern const Image_Typedef mainPValveRed;			/* значок клапана позиция ([8, 58], 159) */
extern const Image_Typedef mainPValveBlue;
extern const Image_Typedef mainPMenuButton;			/* кнопка меню позиция (240, 0) */
extern const Image_Typedef mainPFan0Pressed;
extern const Image_Typedef mainPFan1Pressed;
extern const Image_Typedef mainPFan2Pressed;
extern const Image_Typedef mainPFan3Pressed;
extern const Image_Typedef mainPMenuButtonPressed;
extern const Image_Typedef mainPSunnyPressed;
extern const Image_Typedef mainPSnowflakePressed;
extern const Image_Typedef settingsPExitButton;		/* позиция 8, 195 */
extern const Image_Typedef settingsPLeftButton;		/* 136, 197 */
extern const Image_Typedef settingsPRightButton;		/* 275, 197 */
extern const Image_Typedef settingsPExitButtonPressed;		/* позиция 14, 200 */
extern const Image_Typedef settingsPLeftButtonPressed;		/* 141, 201 */
extern const Image_Typedef settingsPRightButtonPressed;		/* 281, 201 */
extern const Image_Typedef settingsPProfile;				/* 23 55 */
extern const Image_Typedef settingsPTemperature;				/* 141 52 */
extern const Image_Typedef settingsPTime;				/* 235 55 */
extern const Image_Typedef settingsPProfilePressed;				/* 26 60 */
extern const Image_Typedef settingsPTemperaturePressed;				/* 144 58 */
extern const Image_Typedef settingsPTimePressed;				/* 240 62 */
extern const Image_Typedef settingsPGraph;				/* 23 54 */
extern const Image_Typedef settingsPMCU;				/* 120 58 */
extern const Image_Typedef settingsPProtection;				/* 243 56 */
extern const Image_Typedef settingsPGraphPressed;				/* 27 63 */
extern const Image_Typedef settingsPMCUPressed;				/* 126 66 */
extern const Image_Typedef settingsPProtectionPressed;				/* 246 60 */
extern const Image_Typedef settingsPReady;				/* 28 57 */
extern const Image_Typedef settingsPService;				/* 127 58 */
extern const Image_Typedef settingsPManufacture;				/* 237 53 */
extern const Image_Typedef settingsPReadyPressed;				/* 31 64 */
extern const Image_Typedef settingsPServicePressed;				/* 130 63 */
extern const Image_Typedef settingsPManufacturePressed;				/* 242 59 */
extern const Image_Typedef settingsPProfileTitle;				/* 26 121 */
extern const Image_Typedef settingsPTemperatureTitle;				/* 121 122 */
extern const Image_Typedef settingsPTimeTitle;				/* 236 122 */
extern const Image_Typedef settingsPGraphTitle;				/* 23 122 */
extern const Image_Typedef settingsPMCUTitle;				/* 121 122 */
extern const Image_Typedef settingsPProtectionTitle;				/* 244 122 */
extern const Image_Typedef settingsPReadyTitle;				/* 20 122 */
extern const Image_Typedef settingsPServiceTitle;				/* 128 122 */
extern const Image_Typedef settingsPManufactureTitle;				/* 237 122 */
extern const Image_Typedef profilePECO;					/* 23 55 */
extern const Image_Typedef profilePComfort;				/* 121 55 */
extern const Image_Typedef profilePProtection;			/* 228 54 */
extern const Image_Typedef profilePECOPressed;			/* 26 60 */
extern const Image_Typedef profilePComfortPressed;		/* 128 61 */
extern const Image_Typedef profilePProtectionPressed;	/* 236 60 */
extern const Image_Typedef profilePECOTitle;					/* 26 122 */
extern const Image_Typedef profilePComfortTitle;				/* 132 122 */
extern const Image_Typedef profilePProtectionTitle;				/* 240 123 */
extern const Image_Typedef profilePProfileTitle;				/* 112 6 */
extern const Image_Typedef profilePECOSelected;				/* 26 60 */
extern const Image_Typedef profilePComfortSelected;			/* 128 61 */
extern const Image_Typedef profilePProtectionSelected;		/* 236 60 */
extern const Image_Typedef profilePGraph1Title;				/* 26 123 */
extern const Image_Typedef profilePGraph2Title;				/* 132 123 */
extern const Image_Typedef profilePGraph3Title;				/* 240, 123 */
extern const Image_Typedef profilePSchedule;				/* 22 55 */
extern const Image_Typedef profilePScheduleTitle;			/* 13 122 */
extern const Image_Typedef profilePGraphSelected;
extern const Image_Typedef profilePScheduleSelected;	/* 28 65 */
extern const Image_Typedef menuArrowLeft;	/* 8 76 */
extern const Image_Typedef menuArrowRight;	/* 302 76 */
extern const Image_Typedef profilePSchedulePressed;
extern const Image_Typedef timePMenuTitle;	/* 103 7 */
extern const Image_Typedef timePDay;		/* 22 55 */
extern const Image_Typedef timePNight;		/* 131 55 */
extern const Image_Typedef timePDayTitle;	/* 31 122 */
extern const Image_Typedef timePNightTitle;	/* 138 122 */
extern const Image_Typedef timePTimeTitle;	/* 236 121 */
extern const Image_Typedef timePDate;		/* 22 57 */
extern const Image_Typedef timePDateTitle;	/* 22 121 */
extern const Image_Typedef timePDayPressed;		/* 26 61 */
extern const Image_Typedef timePNightPressed;		/* 134 61 */
extern const Image_Typedef timePDatePressed;		/* 27 63 */
extern const Image_Typedef timePUpButton;	/* 187 128 */
extern const Image_Typedef timePDownButton;	/* 235 128 */
extern const Image_Typedef timePUpButtonPressed;	/* 39 128 */
extern const Image_Typedef timePDownButtonPressed;	/* 86 128 */
extern const Image_Typedef timePOk;		/* 239 202 */
extern const Image_Typedef timePCancel;	/* 122 202 */
extern const Image_Typedef timePOkPressed;		/* 242 202 */
extern const Image_Typedef timePCancelPressed;	/* 129 203 */
extern const Image_Typedef daySetPMenuTitle;	/* 123 6 */
extern const Image_Typedef nightSetPMenuTitle;	/* 123 7 */
extern const Image_Typedef timeSetPMenuTitle;	/* 112 7 */
extern const Image_Typedef dateSetPMenuTitle;	/* 114 6 */
extern const Image_Typedef dateSetPYearTitle;	/* 46 35 */
extern const Image_Typedef dateSetPMonthTitle;	/* 141 36 */
extern const Image_Typedef dateSetPDayTitle;	/* 249 36 */
extern const Image_Typedef graphPApplyButton;			/* 205 201 */
extern const Image_Typedef graphPApplyButtonPressed;	/* 213 2-3 */
extern const Image_Typedef graphPDeleteButton;			/* 8 203 */
extern const Image_Typedef graphPDeleteButtonPressed;	/* 15 204 */
extern const Image_Typedef graphPMiddleButton;			/* 51 144 */
extern const Image_Typedef graphPMiddleButtonPressed;			/* 51 144 */
extern const Image_Typedef graphPIntervalTitle;			/* 200 113 */
extern const Image_Typedef graphPProtectionTitle;		/* 41 124 */
extern const Image_Typedef graphPECOTitle;				/* 56 123 */
extern const Image_Typedef graphPComformTitle;			/* 35 124 */
extern const Image_Typedef graphPFullBar;				/* начало столбоцов 10 37 расстояние до следующего 12 */
extern const Image_Typedef graphPHalfBar;
extern const Image_Typedef graphPEmptyBar;
extern const Image_Typedef graphPBarCursor;				/* сдвиг вниз 2 пикселя с конца столбца, сдвиг вправо 2 пикселя */
extern const Image_Typedef graphPLeftButton;			/* 211 144 */
extern const Image_Typedef graphPLeftButtonPressed;			/* 211 144 */
extern const Image_Typedef graphPRightButton;			/* 255 144 */
extern const Image_Typedef graphPRightButtonPressed;	/* 255 144 */
extern const Image_Typedef graphPDownButton;			/* 8 144 */
extern const Image_Typedef graphPDownButtonPressed;			/* 8 144 */
extern const Image_Typedef graphPUpButton;				/* 90 144 */
extern const Image_Typedef graphPUpButtonPressed;				/* 90 144 */
extern const Image_Typedef graphPGraph1MenuTitle;	/* 135 7 */
extern const Image_Typedef graphPGraph2MenuTitle;	/* 135 7 */
extern const Image_Typedef graphPGraph3MenuTitle;	/* 135 7 */
extern const Image_Typedef graphPMonMenuTitle;		/* 122 7 */
extern const Image_Typedef graphPTueMenuTitle;		/* 135 7*/
extern const Image_Typedef graphPWedMenuTitle;		/* 141 7 */
extern const Image_Typedef graphPThuMenuTitle;		/* 136 7 */
extern const Image_Typedef graphPFriMenuTitle;		/* 135 7 */
extern const Image_Typedef graphPSatMenuTitle;		/* 136 6 */
extern const Image_Typedef graphPSunMenuTitle;		/* 123 7 */
extern const Image_Typedef schedulePMenuTitle;		/* 55 6 */
extern const Image_Typedef schedulePMonTitle;		/* 14 122 */
extern const Image_Typedef schedulePTueTitle;		/* 134 122 */
extern const Image_Typedef schedulePWedTitle;		/* 249 122 */
extern const Image_Typedef schedulePThuTitle;		/* 30 122 */
extern const Image_Typedef schedulePFriTitle;		/* 135 122 */
extern const Image_Typedef schedulePSatTitle;		/* 135 122 */
extern const Image_Typedef schedulePSunTitle;		/* 15 122 */
extern const Image_Typedef graphsSettingsPMenuTitle;	/* 101 7 */
extern const Image_Typedef graphsSettingsPGraph1Title;	/* 26 122 */
extern const Image_Typedef graphsSettingsPGraph2Title;	/* 133 122 */
extern const Image_Typedef graphsSettingsPGraph3Title;	/* 21 122 */
extern const Image_Typedef graphsSettingsPScheduleTitle;	/* 240 122 */
extern const Image_Typedef mcuSettingsPMenuTitle;		/* 91 7 */
extern const Image_Typedef mcuSettingsPFanSpeed;		/* 21 54 */
extern const Image_Typedef mcuSettingsPFanSpeedPressed;		/* 25 63 */
extern const Image_Typedef mcuSettingsPFanSpeedTitle;		/* 20 122 */
extern const Image_Typedef fanSettingsPMenuTitle;		/* 67 7 */
extern const Image_Typedef fanSettingsPMinSpeed;		/* 13 63 */
extern const Image_Typedef fanSettingsPMedSpeed;		/* 121 63 */
extern const Image_Typedef fanSettingsPMaxSpeed;		/* 228 63 */
extern const Image_Typedef fanSettingsPMinSpeedTitle;		/* 11 121 */
extern const Image_Typedef fanSettingsPMedSpeedTitle;		/* 133 121 */
extern const Image_Typedef fanSettingsPMaxSpeedTitle;		/* 224 121 */
extern const Image_Typedef fanSettingsPMinSpeedPressed;		/* 19 68 */
extern const Image_Typedef fanSettingsPMedSpeedPressed;		/* 124 68 */
extern const Image_Typedef fanSettingsPMaxSpeedPressed;		/* 223 68 */
extern const Image_Typedef fanSettingsPUpButton;		/* 90 120  size 70 60 */
extern const Image_Typedef fanSettingsPDownButton;		/* 160 120 */
extern const Image_Typedef fanSettingsPUpButtonPressed;
extern const Image_Typedef fanSettingsPDownButtonPressed;
extern const Image_Typedef fanMinSpeedPMenuTitle;		/* 92 7 */
extern const Image_Typedef fanMedSpeedPMenuTitle;		/* 108 7 */
extern const Image_Typedef fanMaxSpeedPMenuTitle;		/* 90 7 */
extern const Image_Typedef tempSettingsPMenuTitle;		/* 91 7 */
extern const Image_Typedef tempSettingsPComfort;		/* 15 59 */
extern const Image_Typedef tempSettingsPEco;			/* 129 64 */
extern const Image_Typedef tempSettingsPComfortTitle;		/* 15 122 */
extern const Image_Typedef tempSettingsPEcoMinTitle;			/* 121 122 */
extern const Image_Typedef tempSettingsPEcoMaxTitle;			/* 229 122 */
extern const Image_Typedef tempSettingsPProtectionMin;	/* 18 61 */
extern const Image_Typedef tempSettingsPProtectionMax;	/* 123 62 */
extern const Image_Typedef tempSettingsPProtectionMinTitle;	/* 15 122 */
extern const Image_Typedef tempSettingsPProtectionMaxTitle;	/* 121 122 */
extern const Image_Typedef tempSettingsPComfortPressed;			/* 19 63 */
extern const Image_Typedef tempSettingsPEcoPressed;			/* 134 71 */
extern const Image_Typedef tempSettingsPProtectionMinPressed;			/* 121 122 */
extern const Image_Typedef tempSettingsPProtectionMaxPressed;			/* 127 69 */
extern const Image_Typedef comfortTempPMenuTitle;		/* 96 7 */
extern const Image_Typedef ecoMinTempPMenuTitle;		/* 81 7 */
extern const Image_Typedef ecoMaxTempPMenuTitle;		/* 79 6 */
extern const Image_Typedef protectionMinTempPMenuTitle;		/* 71 7 */
extern const Image_Typedef protectionMaxTempPMenuTitle;		/* 71 7 */
extern const Image_Typedef pinCodePZero;	/* 9 81 размер 50 46  */
extern const Image_Typedef pinCodePOne;		/* 60 81 размер 50 46  */
extern const Image_Typedef pinCodePTwo;		/* 111 81 */
extern const Image_Typedef pinCodePThree;	/* 162 81 */
extern const Image_Typedef pinCodePFour;	/* 213 81 */
extern const Image_Typedef pinCodePFive;	/* 9 128 */
extern const Image_Typedef pinCodePSix;		/* 60 128 */
extern const Image_Typedef pinCodePSeven;		/* 111 128 */
extern const Image_Typedef pinCodePEight;		/* 162 128 */
extern const Image_Typedef pinCodePNine;		/* 213 128 */
extern const Image_Typedef pinCodePBackspase;		/* 265 81 */
extern const Image_Typedef pinCodePZeroPressed;		/* 9 81 размер 50 46  */
extern const Image_Typedef pinCodePOnePressed;		/* 60 81 размер 50 46  */
extern const Image_Typedef pinCodePTwoPressed;		/* 111 81 */
extern const Image_Typedef pinCodePThreePressed;	/* 162 81 */
extern const Image_Typedef pinCodePFourPressed;		/* 213 81 */
extern const Image_Typedef pinCodePFivePressed;		/* 9 128 */
extern const Image_Typedef pinCodePSixPressed;		/* 60 128 */
extern const Image_Typedef pinCodePSevenPressed;		/* 111 128 */
extern const Image_Typedef pinCodePEightPressed;		/* 162 128 */
extern const Image_Typedef pinCodePNinePressed;			/* 213 128 */
extern const Image_Typedef pinCodePBackspasePressed;	/* 265 81 */
extern const Image_Typedef servicePinPMenuTitle;		/* 88 7 */
extern const Image_Typedef protectionPMenuTitle;		/* 137 5 */
extern const Image_Typedef protectionPBlockOn;			/* 30 54 */
extern const Image_Typedef protectionPBlockOff;			/* 136 54 */
extern const Image_Typedef protectionPBlockPin;			/* 244 54 */
extern const Image_Typedef protectionPBlockOnTitle;		/* 16 122 */
extern const Image_Typedef protectionPBlockOffTitle;	/* 123 122 */
extern const Image_Typedef protectionPBlockPinTitle;			/* 231 122 */
extern const Image_Typedef protectionPBlockOnPressed;			/* 34 60 */
extern const Image_Typedef protectionPBlockOffPressed;			/* 141 60 */
extern const Image_Typedef protectionPBlockPinPressed;			/* 248 60 */
extern const Image_Typedef protectionPBlockOnSelected;			/* 34 58 */
extern const Image_Typedef protectionPBlockOffSelected;			/* 140 60 */
extern const Image_Typedef restoreSettingsPMenuTitle;		/* 101 7 */
extern const Image_Typedef restoreSettingsPAttentionSign;		/* 125 50 */
extern const Image_Typedef restoreSettingsPTitle;		/* 32 126 */
extern const Image_Typedef servicePMenuTitle;			/* 97 7 */
extern const Image_Typedef servicePTemp;				/* 16 56 */
extern const Image_Typedef servicePSystem;				/* 124 54 */
extern const Image_Typedef servicePMode;				/* 229 59 */
extern const Image_Typedef servicePTempTitle;			/* 14 122 */
extern const Image_Typedef servicePSystemTitle;			/* 132 121 */
extern const Image_Typedef servicePModeTitle;			/* 243 122 */
extern const Image_Typedef servicePOutputs;				/* 25 52 */
extern const Image_Typedef servicePFans;				/* 119 56 */
extern const Image_Typedef servicePOutputsTitle;		/* 11 122 */
extern const Image_Typedef servicePFansTitle;			/* 118 122 */
extern const Image_Typedef servicePTempPressed;			/* 22 60 */
extern const Image_Typedef servicePSystemPressed;		/* 127 60 */
extern const Image_Typedef servicePModePressed;			/* 234 64 */
extern const Image_Typedef servicePOutputsPressed;		/* 30 59 */
extern const Image_Typedef servicePFansPressed;			/* 123 63 */
extern const Image_Typedef serviceTempPComfortTitle;	/* 11 120 */
extern const Image_Typedef serviceTempPEcoMinTitle;		/* 117 121 */
extern const Image_Typedef serviceTempPEcoMaxTitle;		/* 222 120 */
extern const Image_Typedef serviceTempPProtectionMinTitle;	/* 18 120 */
extern const Image_Typedef serviceTempPProtectionMaxTitle;	/* 124 121 */
extern const Image_Typedef serviceModePMenuTitle;			/* 117 7 */
extern const Image_Typedef serviceModePHeating;			/* 22 57 */
extern const Image_Typedef serviceModePCooling;			/* 132 59 */
extern const Image_Typedef serviceModePManual;			/* 235 55 */
extern const Image_Typedef serviceModePHeatingTitle;			/* 24 122 */
extern const Image_Typedef serviceModePCoolingTitle;			/* 122 122 */
extern const Image_Typedef serviceModePManualTitle;			/* 230 121 */
extern const Image_Typedef serviceModePHeatingPressed;			/* 23 59 */
extern const Image_Typedef serviceModePCoolingPressed;			/* 135 62 */
extern const Image_Typedef serviceModePManualPressed;			/* 238 62 */
extern const Image_Typedef serviceModePHeatingSelected;			/* 23 59 */
extern const Image_Typedef serviceModePCoolingSelected;			/* 135 62 */
extern const Image_Typedef serviceModePManualSelected;			/* 238 62 */
extern const Image_Typedef serviceSystemPMenuTitle;				/* 115 6 */
extern const Image_Typedef serviceSystemPTwoPipesTitle;			/* 21 120 */
extern const Image_Typedef serviceOutputsPMenuTitle;			/* 92 7 */
extern const Image_Typedef serviceOutputsPOutput;				/* 18 59 */
extern const Image_Typedef serviceOutputsPOutputQ1Title;		/* 24 122 */
extern const Image_Typedef serviceOutputsPOutput1Title;			/* 136 122 */
extern const Image_Typedef serviceOutputsPOutputPressed;		/* 22 63 */
extern const Image_Typedef outputQ1PMenuTitle;			/* 130 6 */
extern const Image_Typedef outputQ1POutputTypeTitle;	/* 19 122 */
extern const Image_Typedef outputQ1PHystHeating;		/* 125 55 */
extern const Image_Typedef outputQ1PHystCooling;		/* 238 55 */
extern const Image_Typedef outputQ1PHystHeatingTitle;		/* 126 122 */
extern const Image_Typedef outputQ1PHystCoolingTitle;		/* 228 123 */
extern const Image_Typedef outputQ1PHystHeatingPressed;		/* 127 59 */
extern const Image_Typedef outputQ1PHystCoolingPressed;		/* 240 59 */
extern const Image_Typedef outputTypePMenuTitle;		/* 126 7 */
extern const Image_Typedef outputTypePHeatingTitle;		/* 25 122 */
extern const Image_Typedef outputTypePCoolingTitle;		/* 122 122 */
extern const Image_Typedef outputTypePOffTitle;		/* 2 122 */
extern const Image_Typedef outputTypePHeating;		/* 18 60 */
extern const Image_Typedef outputTypePOff;			/* 232 64 */
extern const Image_Typedef outputTypePHeatingSelected;	/* 125 60 */
extern const Image_Typedef outputTypePOffSelected;				/* 232 64 */
extern const Image_Typedef output1PMenuTitle;			/* 136 7 */
extern const Image_Typedef serviceFansPMenuTitle;			/* 54 7 */
extern const Image_Typedef serviceFansPHeatTempOnTitle;		/* 16 122 */
extern const Image_Typedef serviceFansPHeatRangeTitle;		/* 122 122 */
extern const Image_Typedef serviceFansPCoolTempOnTitle;		/* 226 122 */
extern const Image_Typedef serviceFansPCoolRangeTitle;		/* 12 122 */
extern const Image_Typedef tempComfortDeltaPMenuTitle;				/* 76 6 */
extern const Image_Typedef tempECODeltaMinPMenuTitle;
extern const Image_Typedef tempECODeltaMaxPMenuTitle;				/* 74 7 */
extern const Image_Typedef tempProtectionDeltaMinPMenuTitle;		/* 68 7 */
extern const Image_Typedef tempProtectionDeltaMaxPMenuTitle;		/* 66 6 */
extern const Image_Typedef hystHeatingPMenuTitle;					/* 101 7 */
extern const Image_Typedef hystCoolingPMenuTitle;					/* 92 7 */
extern const Image_Typedef heatingTempOnPMenuTitle;					/* 62 7 */
extern const Image_Typedef coolingTempOnPMenuTitle;					/* 53 6 */
extern const Image_Typedef heatingRangePMenuTitle;					/* 66 7 */
extern const Image_Typedef coolingRangePMenuTitle;					/* 57 6 */
extern const Image_Typedef autoblockPinPMenuTitle;
extern const Image_Typedef enterPinPMenuTitle;
extern const Image_Typedef errorPMenuTitle;
extern const Image_Typedef errorPSensorErrorTitle;


#ifdef __cplusplus
	}
#endif

#endif /* IMAGES_H_ */
