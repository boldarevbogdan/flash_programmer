/*
 * ch341a.h
 *
 *  Created on: 21 янв. 2018 г.
 *      Author: bogdan
 */

#ifndef USBSPICONVERTER_H_
#define USBSPICONVERTER_H_

#include <iostream>
#include <cstdint>
#include <csignal>
#include <thread>
#include <chrono>
#include <libusb-1.0/libusb.h>

 #define CH341A
// #define STM32F1_CONVERTER

#define HIGH	1
#define LOW		0

#define DEFAULT_TIMEOUT        1000

int8_t USBSPIConverterInit (void);
void USBSPIConverterDeinit (void);

/* изменить порядок бит в байте. Ch341a передает байты младшим битом вперед */
uint8_t ReverseBitsOrder (uint8_t byte);

/* дефайны для преобразователя на Stm32F103 */
#define STM32F1_CONVERTER_VID		0x0483
#define STM32F1_CONVERTER_PID		0x7540

#define DATA_OUT_ENDPOINT	0x01
#define DATA_IN_ENDPOINT	0x82
#define COMMAND_ENDPOINT	0x03

/* Команды которые принимает контрольная точка 3 */
#define USB_DEVICE_SET_CONVERTER_MODE		0x01
#define USB_DEVICE_SET_SPEED				0x02
#define USB_DEVICE_SET_BIT_ORDER			0x04
#define USB_DEVICE_SPI_SET_CS_STATE			0x05

/* возможные значения команды USB_DEVICE_SET_CONVERTER_MODE */
#define USB_DEVICE_MODE_SPI 	0
#define	USB_DEVICE_MODE_I2C		1

/* значения скорости для режима SPI */
#define USB_DEVICE_SPI_SPEED_36MHZ 	0
#define USB_DEVICE_SPI_SPEED_18MHZ	1
#define USB_DEVICE_SPI_SPEED_9MHZ	2
#define USB_DEVICE_SPI_SPEED_4MHZ	3
#define USB_DEVICE_SPI_SPEED_2MHZ	4
#define USB_DEVICE_SPI_SPEED_1MHZ 	5
#define USB_DEVICE_SPI_SPEED_500kHZ	6
#define USB_DEVICE_SPI_SPEED_250kHZ	7

/* порядок битов при передачи по SPI */
#define USB_DEVICE_SPI_MSBFIRST		0
#define USB_DEVICE_SPI_LSBFIRST		1

#define STM32F1_CONVERTER_PACKET_LENGTH		64

/* дефайны для программатора на CH341A */
#define CH341A_VID		0x1A86
#define CH341A_PID		0x5512

#define BULK_WRITE_ENDPOINT    0x02
#define BULK_READ_ENDPOINT     0x82

#define CH341A_CMD_SPI_STREAM  0xA8
#define CH341A_CMD_UIO_STREAM  0xAB

#define CH341A_CMD_UIO_STM_DIR 0x40
#define CH341A_CMD_UIO_STM_OUT 0x80
#define CH341A_CMD_UIO_STM_END 0x20

#define CH341_PACKET_LENGTH    0x20			/* максимальный размер пакета равен размеру конечной точки 32 байта */

/* управление пином СS. state - состояние пина, 1 - подтянуть к питанию, 0 - подтянуть к земле */
int CSControl (uint8_t state);

/* записать и прочитать из флешки */
int	SPIWrite (uint8_t *write_data, uint32_t bytes_num);
int SPIRead (uint8_t *read_data, uint32_t bytes_num);

#endif /* USBSPICONVERTER_H_ */
